
import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PreDestroy;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */

/**
 *
 * @author DTSOZAY
 */
public class FindFreqItems {

    /**
     * @param args the command line arguments
     */
    private static HashMap<SortedSet<String>, Integer> countMap = new HashMap<SortedSet<String>, Integer>();
    private static int threshold = 3;
    private static List<SortedSet<String>> transactions;

    public static void main(String[] args) {

        //GenerateFile(1200, 26);


        System.out.println("Please insert threshold value: (Th>0)");
        Scanner in = null;
        try {
            in = new Scanner(System.in);
            threshold = Integer.parseInt(in.nextLine());
            long start = System.currentTimeMillis();
            transactions = GetTransactionFromFile();

            List<SortedSet<SortedSetList>> freqItems = findFrequentItemSets(transactions);

            int i = 1;
            
            for (SortedSet<SortedSetList> item : freqItems) {

                int count = 0;
                System.out.println(i + ".level itemsets:");

                for (SortedSetList s : item) {
                    System.out.print("{");
                    Iterator it = s.GetList().iterator();
                    while (it.hasNext()) {
                        // Get element
                        Object element = it.next();
                        System.out.print(element.toString());
                    }
                    System.out.print("} [" + countMap.get(s.GetList()) + "] ,");
                    count++;
                }
                System.out.println("Count:"+count);
                i++;
            }
            long elapsedTimeMillis = System.currentTimeMillis() - start;
            System.out.println("Program ended.Please press <Enter> to finish.. Total tıme:" + elapsedTimeMillis / (1000F));
            in.nextLine();
        } catch (Exception e) {
            System.out.println(e.toString());
        } finally {
            in.close();
        }
    }

    private static List<SortedSet<String>> GetTransactionFromFile() {
        try {

            FileInputStream fstream = new FileInputStream("data.txt");

            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;

            List<SortedSet<String>> trans = new ArrayList<SortedSet<String>>();
            while ((strLine = br.readLine()) != null) {

                SortedSet<String> items = new TreeSet<String>();
                String[] values = strLine.split(",");
                for (int i = 0; i < values.length; i++) {
                    if (values[i].length() > 0) {
                        items.add(values[i]);
                    }
                }
                trans.add(items);
            }

            in.close();
            return trans;
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
            return null;
        }
    }

    private static List<SortedSet<SortedSetList>> findFrequentItemSets(List<SortedSet<String>> transactions) {

        List<SortedSet<SortedSetList>> freqItems = new ArrayList<SortedSet<SortedSetList>>();
        SortedSet<SortedSetList> level1itemSet = get1LevelItemSet(transactions);
        level1itemSet = clear1ItemsByTreshold(level1itemSet);
        freqItems.add(level1itemSet);

        calculateItemSetOver1(level1itemSet, freqItems, 2);

        return freqItems;
    }

    private static SortedSet<SortedSetList> get1LevelItemSet(List<SortedSet<String>> transactions) {
        SortedSet<SortedSetList> level1itemSet = new TreeSet<SortedSetList>();
        for (SortedSet<String> transaction : transactions) {
            for (String value : transaction) {
                SortedSet<String> s = new TreeSet<String>();
                s.add(value);
                SortedSetList k = new SortedSetList(s);
                level1itemSet.add(k);
                if (!countMap.containsKey(s)) {
                    countMap.put(s, 1);
                } else {
                    countMap.put(s, countMap.get(s) + 1);
                }
            }
        }
        return level1itemSet;
    }

    private static SortedSet<SortedSetList> clear1ItemsByTreshold(SortedSet<SortedSetList> itemSets) {
        SortedSet<SortedSetList> items = new TreeSet<SortedSetList>();
        for (SortedSetList item : itemSets) {
            if (countMap.get(item.GetList()) >= threshold) {
                items.add(item);
            }
        }
        return items;
    }
    
    private static SortedSet<SortedSetList> clearAprioriesByThreshold(SortedSet<SortedSetList> apiriories) {
        SortedSet<SortedSetList> result = new TreeSet<SortedSetList>();
        for (SortedSetList set : apiriories) {
            if (countMap.containsKey(set.GetList())) {
                if (countMap.get(set.GetList()) >= threshold) {
                    result.add(set);
                }
            }
        }

        return result;
    }

    private static void calculateItemSetOver1(SortedSet<SortedSetList> levelItemSet, List<SortedSet<SortedSetList>> freqItems, int entryLevel) {
        SortedSet<SortedSetList> apiriories = apriori_genwithArray(levelItemSet, entryLevel);
        if (apiriories == null || apiriories.size() == 0) {
            System.out.println(entryLevel + ". level apriories is not exsist");
            return;
        }
        /*
         * apiriories = clearInfrequentSubsets(apiriories); if (apiriories ==
         * null || apiriories.size() == 0) { System.out.println(entryLevel + ".
         * level apriories is not exsist"); return;
        }
         */
        updateCountWithSearchInTransactions(apiriories);
        apiriories = clearAprioriesByThreshold(apiriories);
        if (apiriories == null || apiriories.size() == 0) {
            System.out.println(entryLevel + ". level apriories is not exsist");
            return;
        }
        freqItems.add(apiriories);
        System.err.println("Calculating level " + entryLevel);
        calculateItemSetOver1(apiriories, freqItems, entryLevel + 1);
    }

    private static SortedSet<SortedSetList> apriori_gen(SortedSet<SortedSetList> levelItemSet, int entryLevel) {
        SortedSet<SortedSetList> result = new TreeSet<>();

        // Combinations c = new Combinations(boys, 3);,
        System.out.println("level:" + entryLevel + " started");
        for (SortedSetList prApri : levelItemSet) {
            SortedSet<String> prS = prApri.GetList();
            long start = System.currentTimeMillis();
            for (SortedSetList prApriMirror : levelItemSet) {
                SortedSet<String> prSM = prApriMirror.GetList();

                Iterator<String> iprS = prS.iterator();
                Iterator<String> iprSM = prSM.iterator();

                int i = 0;
                while (iprS.hasNext() && iprSM.hasNext()) {
                    // Get element
                    i++;
                    String element = iprS.next();
                    String elementM = iprSM.next();

                    if (element.equals(elementM)) {
                        continue;
                    } else if (element.compareTo(elementM) < 0 && i == entryLevel - 1) {
                        SortedSetList item = merge(prS, elementM);
                        if (clearInfrequentSubset(item)) {
                            result.add(merge(prS, elementM));
                        }
                    }
                }
            }
            if (entryLevel > 4) {
                //System.out.println("level:" + entryLevel + " time:" + (System.currentTimeMillis() - start));
            }
            

        }
        System.out.println("level:" + entryLevel + " end");
        return result;
    }
    
    private static String[] MergeArrays(String[] source, String target[])
    {
        String[] finalA = null;
        for(int i = 0; i< source.length;i++)
        {
            if(source[i] != target[i] && i+1 < source.length)
                break;
            else if(source[i] != target[i] && i+1 == source.length)
            {
                finalA = new String[source.length+1];
                for(int k=0; k<source.length+1 ;k++)
                {
                    if(k == source.length)
                        finalA[k] = target[i];
                    else
                        finalA[k] = source[k];
                }
            }
        }
        return finalA;
        
    }
    
     private static SortedSet<SortedSetList> apriori_genwithArray(SortedSet<SortedSetList> levelItemSet, int entryLevel) {
        SortedSet<SortedSetList> result = new TreeSet<>();

        List<String[]> arItems = new  ArrayList<String[]>(levelItemSet.size());
        
        for(SortedSetList prApri : levelItemSet)
        {
            String[] items = new String[entryLevel-1];
            prApri.GetList().toArray(items);
            arItems.add(items);
        }
        
        for(String[] s : arItems)
        {
            for(String[] k : arItems)
            {
                if(k == s)
                    continue;
                else 
                {
                    String[] fin = MergeArrays(s, k);
                    if(fin != null)
                    {
                        SortedSet<String> sorted = new TreeSet<String>();
                        for(int i=0; i<fin.length;i++)
                             sorted.add(fin[i]);
                    result.add(new SortedSetList(sorted));    
                    }
                }                    
            }
        }
    
        System.out.println("level:" + entryLevel + " end");
        return result;
    }
    
    private static SortedSet<SortedSetList> apriori_genTest(SortedSet<SortedSetList> levelItemSet, int entryLevel) {
        SortedSet<SortedSetList> result = new TreeSet<>();

        // Combinations c = new Combinations(boys, 3);
        for (SortedSetList prApri : levelItemSet) {
            SortedSet<String> prS = prApri.GetList();
            long start = System.currentTimeMillis();
            for (SortedSetList prApriMirror : levelItemSet) {
                SortedSet<String> prSM = prApriMirror.GetList();
                if(prS == prSM) continue;
                
                
                Iterator<String> iprSM = prSM.iterator();

                int i = 0;
                while (iprSM.hasNext()) {
                    // Get element
                    i++;
                    
                    String elementM = iprSM.next();

                    if (prS.contains(elementM)) {
                        continue;
                    } else 
                    {
                        SortedSetList item = merge(prS, elementM);
                        if (!result.contains(item) && clearInfrequentSubset(item)) {
                            result.add(merge(prS, elementM));
                        }
                    }
                }
            }
            if (entryLevel > 4) {
                System.out.println("level:" + entryLevel + " time:" + (System.currentTimeMillis() - start));
            }

        }
        return result;
    }


    private static SortedSetList merge(SortedSet<String> prS, String elementM) {

        SortedSet<String> result = new TreeSet<String>();
        result.addAll(prS);
        result.add(elementM);
        return new SortedSetList(result);
    }

    private static SortedSet<SortedSetList> clearInfrequentSubsets(SortedSet<SortedSetList> apiriories) {

        SortedSet<SortedSetList> result = new TreeSet<SortedSetList>();

        for (SortedSetList apr : apiriories) {
            SortedSet<String> apriori = apr.GetList();
            Boolean state = true;
            for (int i = 0; i < apriori.size(); i++) {
                //remove one by one 
                Object[] keys = apriori.toArray();
                SortedSet<String> look = new TreeSet<String>();
                for (int k = 0; k < keys.length; k++) {
                    if (k != i) {
                        look.add(keys[k].toString());
                    }
                }
                //burası referans bulmada sorun çıkarabilir
                if (!countMap.containsKey(look)) {
                    state = false;
                }
            }
            if (state) {
                result.add(apr);
            }
        }

        return result;
    }

    private static Boolean clearInfrequentSubset(SortedSetList apr) {
        SortedSet<String> apriori = apr.GetList();
        Boolean state = true;
        for (int i = 0; i < apriori.size(); i++) {
            //remove one by one 
            Object[] keys = apriori.toArray();
            SortedSet<String> look = new TreeSet<String>();
            for (int k = 0; k < keys.length; k++) {
                if (k != i) {
                    look.add(keys[k].toString());
                }
            }
            //burası referans bulmada sorun çıkarabilir
            if (!countMap.containsKey(look)) {
                state = false;
            }
        }
        return state;
    }

    private static void updateCountWithSearchInTransactions(SortedSet<SortedSetList> apiriories) {

        for (SortedSetList apr : apiriories) {
            SortedSet<String> apriori = apr.GetList();
            for (SortedSet<String> transaction : transactions) {
                if (transaction.containsAll(apriori)) {
                    if (!countMap.containsKey(apriori)) {
                        countMap.put(apriori, 1);
                    } else {
                        countMap.put(apriori, countMap.get(apriori) + 1);
                    }
                }
            }
        }
    }


    private static void GenerateFile(int lineNumber, int alphabetLenght) {
        try {
            // Create file 
            FileWriter fstream = new FileWriter("data.txt");
            BufferedWriter out = new BufferedWriter(fstream);

            lineNumber = lineNumber;

            Random rItem = new Random();
            Random rTransItemCount = new Random();

            String alphabet = "qwertyuıopasdfghjklzxcvbnm";
            alphabet = alphabet.substring(0, alphabetLenght);
            for (int k = 0; k < lineNumber; k++) {
                String line = "";
                int itemCount = rItem.nextInt(alphabet.length());
                if (itemCount == 0) {
                    continue;
                }
                HashSet<Integer> values = new HashSet<Integer>();
                for (int j = 0; j < itemCount; j++) {
                    int rnd = rItem.nextInt(alphabet.length());
                    if (values.contains(rnd)) {
                        itemCount++;
                        continue;
                    }
                    values.add(rnd);
                    line += alphabet.charAt(rnd) + ",";
                }
                out.write(line.substring(0, line.length() - 1));
                out.newLine();
            }

            out.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }
}
class SortedSetList implements Comparable<SortedSetList> {

    private SortedSet<String> local;

    public SortedSet<String> GetList() {
        return local;
    }

    SortedSetList(SortedSet<String> s) {
        local = s;
    }

    public int compareTo(SortedSetList remote) {

        int result = 0;

        Iterator itRemote = remote.GetList().iterator();
        Iterator itlocal = local.iterator();
        while (itRemote.hasNext() && itlocal.hasNext()) {
            // Get element
            Object elementO = itRemote.next();
            Object elementL = itlocal.next();

            if (elementL.toString().equals(elementO.toString())) {
                continue;
            } else {
                return elementL.toString().compareTo(elementO.toString());
            }
        }

        return result;
    }
}